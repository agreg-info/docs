---
title: Installer NonOS
layout: page
---

> Informations à destination des candidats et des préparateurs au concours

* Table des matières
{:toc}

L'environnement est basé sur la dernière version LTS d'Ubuntu (cf. [FAQ](faq.html)) dans laquelle les paquets dont la présence est prévue au concours sont préinstallés, et certaines opérations de configuration sont réalisées.

Nous mettons à disposition diverses options pour tester l'environnement : machine virtuelle OVA, image disque ISO.

# Différences principales avec l'environnement de concours

- Dans cette démo, vous avez accès à Internet. Pas le jour du concours.
- Ici, l'utilisateur `candidat` a pour mot de passe `concours` et est administrateur. Le jour J, le compte du candidat au concours ne sera évidemment pas administrateur.

# Machine virtuelle OVA avec VirtualBox

C'est l'option la plus simple. Pour l'installer :

- Téléchargez [la machine virtuelle au format compressé OVA ici](https://gitlab.com/agreg-info/clef-agreg/-/releases) (5 Go)
- Lancez VirtualBox, utilisable depuis n'importe quelle plateforme.
- Fichier > Importer un appareil virtuel > Le fichier OVA.
- Double-cliquez sur NonOS, la VM devrait démarrer.
- L'utilisateur `candidat` a pour mot de passe `concours`.

Par défaut, la RAM utilise 4GB. Pour changer la résolution de la machine virtuelle, tapez par exemple `xrandr -s 1440x900` dans un terminal. Pour augmenter la résolution maximale, vous pouvez installer les [Guest Additions](https://www.virtualbox.org/manual/ch04.html) pour votre version de VirtualBox.

# Image ISO à graver sur clé

L'image ISO pré-générée avec QEMU est disponible au téléchargement [ici](https://gitlab.com/agreg-info/clef-agreg/-/releases).

Il est techniquement possible de `cat` ou de `dd` l'image ISO sur une clef USB, mais son poids élevé (actuellement 13 Go) nécessite une clé d'au moins 16 Go, à cause par exemple de :

- Zeal (2 Go)
- LaTeX (~1 Go)
- Python (1 Go)
- opam, the OCaml Package Manager (1 Go)

Installer ou utiliser [Xubuntu](https://xubuntu.org/download/) (Ubuntu avec xfce) sur votre poste de travail pourra être une manière plus légère (2,9 Go) de s'y habituer.

# Sources de génération

Les sources de génération de la VM sont [sur GitLab](https://gitlab.com/agreg-info/clef-agreg/).
