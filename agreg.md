---
title: Setup de l'agrégation d'informatique
layout: page
---

> Informations sur le setup de l'agrégation d'informatique

* Table des matières
{:toc}




# Architecture générale

![Architecture](clientserver.svg)

Sur un VLAN dédié, un serveur contrôle centralement tous les différents PC liés au concours : les ordinateurs sur lesquels composent les candidats, ainsi que les ordinateurs des salles de passage. Ce serveur contient en particulier toutes les données de tous les candidats. Nous avons pu avoir une isolation physique totale sur le réseau interne : notre serveur est directement lié à des switchs dans les baies de brassage du bâtiment de l'agrégation, liées aux prises réseau utilisées par nos laptops, la seule connexion "extérieure" étant sur une autre interface du serveur (qui sert de routeur).

## Proxy

L'uplink du lycée doit passer par un proxy http (`http://10.0.0.1:3128`) administré par (probablement) la région Île-de-France qui gère le lycée, et perturbant l'installation à chaque étape pouvant inclure des appels réseau. Le setup présenté ici peut fonctionner intégralement en présence d'un tel proxy.


# Installation du serveur

Le serveur a été installé avec un iso de Ubuntu 22.04 Server, non modifié, puis avec les paquets suivants :

`apt install iptables-persistent isc-dhcp-server ca-certificates curl gnupg lsb-release python3-pip nfs-kernel-server autofs chrony freeipa-client tree net-tools pxelinux apt-cacher-ng apache2 tftpd-hpa plocate iftop iotop ncdu acl`

Après avoir ajouté/signé le repo `deb https://download.docker.com/linux/ubuntu jammy stable` (avec [sa clef](https://download.docker.com/linux/ubuntu/gpg) ) :

`apt install docker-ce docker-ce-cli containerd.io`

## FreeIPA

[FreeIPA](https://en.wikipedia.org/wiki/FreeIPA) gère l'identité et les politiques de sécurité de manière unifiée et simplifiée, en se basant sur LDAP + Kerberos, avec gestion de NTP et DNS. La manière la plus simple de le gérer est de l'installer dans un [conteneur docker](https://github.com/freeipa/freeipa-container/). Il convient de lire la documentation précise, et notamment pour une bonne gestion des cgroups de mettre `{ "userns-remap": "default" }` dans `/etc/docker/daemon.json` et relancer dockerd.

À ma connaissance il n'y a pas de manière simple de récupérer des images docker à travers un proxy. J'ai donc `docker pull freeipa/freeipa-server:centos-9-stream` sur une machine avec accès direct à internet, et utilisé `docker save` puis `docker load` sur le serveur

## Configuration diverse.

`ansible-playbook server.yml -e "@ansible_variables.json" -i hosts.production.yaml -v`

`hosts.production.yaml` : 
```yaml
{% include_relative lab-architecture/hosts.production.yaml %}

`ansible_variable.json` : 

```json
{% include_relative lab-architecture/ansible_variables.json %}
```

`server.yml` :
<details>
  <summary>Cliquer pour étendre</summary>

{% highlight yaml %}

{% include_relative lab-architecture/server.yml %}

{% endhighlight %}

</details>

## Comptes et skel

Les comptes sont gérés par ipa, créés à partir d'un csv créant une boucle de commandes `echo $password | ipa user-add $username --first $firstname --last $lastname --shell /bin/bash --password &`

De manière importante, chaque /home sera initialisé avec le contenu du /etc/skel du serveur, qu'on modifiera donc pour : 
 - ajouter `(echo "# Backups every 5 min"; echo "*/5 * * * * NOW=\$(date +'\%d-\%m-\%Y-\%H:\%M:\%S'); rdiff-backup --exclude=\"**/.*\" $HOME /var/local/backups/backup-$USER-\$NOW") | crontab -` au .profile pour créer des backups
 - ajouter `export VSCODE_EXTENSIONS=/usr/local/share/codium` au .profile pour pouvoir gérer les extensions de codium par machine, ne pas surcharger inutilement le réseau, et garantir leur présence même sans réseau
 - ajouter `xrandr --output eDP-1 --mode 1920x1080 --output HDMI-2 --mode 1920x1080 --same-as eDP-1 2>/dev/null || true` au `.profile` pour automatiser l'installation au vidéoprojecteur
 - mettre tuareg dans `.emacs.d/`, configurer `.emacs` et `.bashrc` pour le prendre en compte et initialiser opam
 - installer le kernel jupyter de Ocaml dans `.local/share/jupyter/kernels/ocaml-jupyter/kernel.json`
 - Préparer des configurations par défaut raisonnables dans `.config/Zeal/Zeal.conf` (idem que pour codium, pour avoir localement les docsets de Zeal sans les avoir dans les $home), `.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-screensaver.xml` et  `.config/xfce4/xfconf/xfce-perchannel-xml/xfwm.xml`
 - Proposer des raccourcis sur le bureau

```
{{ "lab-architecture/config/skel" | namedtree: "/etc/skel" }}
```

### Considérations de sécurité

Par sécurité, les permissions des dossiers (`/home/$compte`) des candidats sont en permissions `drwxrwx--x+   6 root    candidat`), autrement dit:
 - root possède le dossier, de sorte à ce que le candidat ne puisse pas modifier les droits d'accès pour permettre à d'autres de le lire
 - le groupe de l'utilisateur a les droits `rwx` pour créer et gérer ses fichiers librement
 - le mode o+x est nécessaire au fonctionnement des snaps, snapd ayant besoin d'accéder à `$HOME/snap/…`

 - Idéalement, `/home` devrait être en `drwxr-x--x 104 root     root` mais les snaps [ne fonctionnent pas si c'est le cas](https://bugs.launchpad.net/snapd/+bug/2024342)

Ces permissions permettant théoriquement à un candidat d'accéder à des parties du dossier d'autres candidats (en connaissant en avance le chemin de l'autre fichier), des ACL sont mises en place pour tous les candidats : pour toute paire de candidat i≠j, on pose `setfacl -m u:$i:- /home/$j` ce qui aboutit à la situation

```
getfacl /home/candidat
# file: candidat
# owner: root
# group: candidat
user::rwx
user:autre-candidat:---
user:autre-candidat2:---
group::rwx
mask::rwx
other::--x
```
empêchant effectivement les candidats de `cd /home/autre-candidat`.

## DHCP

La gestion du réseau "interne" est faite par le serveur, qui attribue donc les IP: 
`dhcpd.conf`
```
{% include_relative lab-architecture/templates/dhcp.conf.j2 %}
```

### TFTP

Pas clair que tout soit utile, a nécessité des itérations. De manière importante, initrd *doit* venir du dossier casper/ de l'iso d'installation d'Ubuntu server
```
{{ "lab-architecture/config/tftp" | namedtree: "/srv/tftp" }}
```

### Apache2

```
{{ "lab-architecture/config/http" | namedtree: "/var/www/html" }}
```

## apt-cacher-ng

Pour ne pas récupérer d'internet les packages des installations, on utilise apt-cacher-ng qui les met en cache localement.
Pour l'utiliser, le user-data de l'installation contiendra donc 
```
  apt:
    primary:
        - arches: [default]
          uri: http://10.0.1.1:3142/archive.ubuntu.com/ubuntu/
```

# Installation clients

L'installation se passe en deux temps : un user-data de cloud-config ([exemples](https://cloudinit.readthedocs.io/en/latest/reference/examples.html)) installe le système via PXE, puis une fois les machines installées, un script ansible "termine" de les enroller dans le réseau

## user-data

<details>
  <summary>Cliquer pour étendre</summary>

{% highlight yaml %}

{% include_relative lab-architecture/config/http/nocloud/user-data %}

{% endhighlight %}

</details>


## Ansible

<details>
  <summary>Cliquer pour étendre</summary>

{% highlight yaml %}

{% include_relative lab-architecture/client.yml %}

{% endhighlight %}

</details>

# Backups

Afin de laisser aux candidats et candidats toutes leurs chances, des backups réguliers de leurs données (tout le $HOME) sont prévus : 
 - Sur le client, toutes les cinq minutes, dans un répertoire accessible à tout utilisateur, afin de pouvoir immédiatement dépanner (sur son poste) un candidat ou une candidate ayant accidentellement supprimé des données
 - Sur le serveur, dans un disque séparé, toutes les dix minutes (copie complète de /home), afin d'avoir une copie "de référence" intouchable.

Afin de toujours pouvoir les avoir dans leur version originale, les fichiers fournis avec un sujet (code à auditer, exemples ou templates présents dans le sujet, ou tout fichier que le jury souhaite donner en accès aux candidates et candidats pour une épreuve donnée) sont en général fournis dans $HOME en lecture seule, avec une invitation à en faire une copie si nécessaire.

# En cas de souci
#### Avec un laptop

Des laptops de rechange sont prévus. De par la configuration, aux backups locaux passés près, l'environnement en se connectant sur un laptop ou un autre est indifférenciable.

#### Avec le réseau (ou avec le serveur)

La perte de la connexion réseau, ou du serveur, ont la même conséquence : l'impossibilité d'écrire tout fichier dans le répertoire de la personne. Si le problème n'est pas immédiatement résolu, un compte local non-administrateur est prêt sur les machines, et son mot de passe est donné (ainsi que la procédure pour restaurer les backups locaux). Il est alors demandé de se déplacer devant le jury avec le laptop. Ce compte sauvegarde également son contenu (localement) régulièrement

#### Avec l'électricité

Les laptops ont une batterie de plusieurs heures, mais si la coupure touche également les switchs réseau ou le serveur, le point précédent s'applique.







