---
title: NonOS
layout: page
toc: true
---

> Ce n'est pas un OS, c'est un environnement de concours

* Table des matières
{:toc}

# Introduction

Bienvenue sur ce projet de standardisation d'environnement de concours, utilisé à l'origine lors des oraux de l'agrégation d'informatique en 2022.

Les épreuves pratiques d'informatique dans le cadre de concours, bien qu'anciennes, se sont récemment multipliées :

- au concours des ENS ;
- au CAPES d'informatique ;
- à l'agrégation d'informatique ;
- ainsi que, plus récemment, dans divers concours d'écoles d'ingénieurs suite à la création de la filière MPI.

Dans ce cadre, il nous a semblé important de documenter l'environnement de passage des épreuves, avec plusieurs intentions :

- maximiser la transparence et l'ouverture de la configuration technique de tels environnements ;
- pour les [concours](custom.html), mutualiser dans la mesure du possible l'effort de création et de proposition de l'environnement de concours, ainsi que les détails de sa mise en pratique en production ;
- pour les [candidats](install.html), proposer une interface standard dans laquelle ils passeront leurs concours, afin de s'y préparer et de minimiser l'effort d'adaptation ;
- pour les [préparateurs aux concours et autres enseignants](install.html), publier les outils mis à disposition et tester les environnements.

Nous utilisons donc dans ce cadre au maximum des outils *libres* et *open source*.

# Détails sur l'environnement

## Langages et documentation hors ligne

Les numéros de versions listés sont des versions disponibles immédiatement dans l'image mise à disposition.

- Python 3.10.6, avec :

        numpy==1.21.5
        scipy==1.10.1
        scikit-learn==1.2.2
        matplotlib==3.7.1
        pandas==2.0.1
        Pillow==9.0.1
        imageio==2.28.0
        jupyterlab==3.3.4
        mypy==1.2
        networkx==3.1
        pydot==1.4.2
        pyzo==4.12.7

- OCaml 4.13.1, avec `utop` et `ocaml-lsp-server`
- C et C++ (gcc 11.3) avec `gdb` et `valgrind`
- PHP 8.1.2
- SQLite 3.37.2

Vous pouvez accéder aux documentations hors ligne en tapant `zeal` dans un terminal ou bien [DevDocs](https://devdocs.io/).

        C
        C++
        LaTeX
        Matplotlib
        NumPy
        OCaml
        Python 3
        SQLite
        SciPy

Pour économiser de la place, l'environnement téléchargeable de démonstration ne contient pas toutes les documentations, mais elles seront présentes le jour de l'épreuve.

De nombreuses manpages sont fournies.

## IDEs

### Codium, équivalent libre de VSCode

Tapez `codium` dans un terminal puis :

- Interprétez Python avec `F5`
- Interprétez OCaml avec `Maj+Entrée`
- Compilez C++ avec `F5` (ou `C-Maj-b` puis entrez les options de compilation)

### Jupyter Lab

Démarrez avec `jupyter lab` et vous aurez accès aux noyaux Python et OCaml.

### Autres IDEs

- emacs avec le mode Tuareg `C-c C-e` d'OCaml
- vim
- gedit
- Pyzo 4.12.7
- PyCharm Community Edition

## Autres logiciels

Traitement de texte
:       
LibreOffice, TeX Live 2023 minimal avec notamment les packages beamer, pgf (TikZ) et multido

Web
:       
Firefox, serveur Apache servant `/var/www/html` sur `http://localhost`

Visualisation
:       
Gimp, Inkscape, Graphviz, Gnuplot

Outils pour l'informatique
:       
flex, ml-yacc, ragel, menhir

Gestion de versions
:       
git, svn

# Nous contacter

Nous vous invitons d'abord à consulter [la FAQ](faq.html).

Les questions spécifiques à la clé peuvent être posées à [cle-agreg-info@groupes.renater.fr](mailto:cle-agreg-info@groupes.renater.fr).

# Remerciements

Merci à :

- Rémy Grünblatt pour son aide ;
- Nathaniel Carré, Jean-Christophe Filliâtre, Yann Salmon pour leurs retours ;
- Matthieu Le Floch, François Boisson, Yann Hermans, François Avril, Antoine Crouzet et Raphaël Augris pour leurs conseils et commentaires ;
- tous les contributeurs et candidat·es ayant fait des propositions.
