---
title: Adapter NonOS
layout: page
---

> Informations à destination des organisateurs de concours

# Adapter l'environnement

En théorie, il devrait être assez simple d'adapter l'environnement de test en ajoutant ou retirant des paquets et des scripts au fichier [`cloud-config.yaml`](https://gitlab.com/agreg-info/clef-agreg/-/blob/master/cloud-config.yaml). À terme, il sera envisageable de générer ce fichier pour pouvoir faire des `make ${concours}` différenciés.

Pour en savoir plus, les sources de génération de la VM sont [sur GitLab](https://gitlab.com/agreg-info/clef-agreg/).

# Déployer l'environnement

En fonction des contraintes techniques sur le lieu de concours et sur les modalités de celui-ci, il existe plusieurs manières de gérer le déploiement d'une infrastructure.

Par exemple, si les candidats composent et présentent sur la même machine, il est possible de ne pas utiliser du tout de réseau, d'avoir juste un compte local par machine et d'utiliser une clé USB pour les sauvegardes automatiques.

Les deux questions principales qui se posent sont les suivantes :

## Quelle maîtrise des disques durs ?

Idéalement, pouvoir installer le système sur le disque dur de la machine de concours permet de contrôler complètement le processus depuis le boot, et de s'assurer d'avoir toute place et toute vitesse nécessaire.

Sans ce contrôle, il reste possible de booter sur des clés USB ou des disques externes, avec les risques de mauvaises manipulations associées. Dans certaines situations, l'infrastructure présente sur le site a la possibilité de "booter" sur une image ISO de manière transparente pour l'utilisateur, mais demande une coopération étroite avec la DSI du lieu de concours.

## Quelle maîtrise du réseau ?

Nous supposons qu'il est trop dangereux de laisser une quelconque possibilité de connexion sans fil. Si la machine de concours est un laptop ou possède une carte Wi-Fi, rfkill et rmmod sur les pilotes Wi-Fi.

S'il est souhaitable de pouvoir se connecter à distance de manière simple vers les machines des candidats, pour quelque raison que ce soit (par ex. vérifier à distance que toutes les machines sont allumées et connectées au réseau local), soit les machines sont sur un réseau dédié, soit des règles strictes de routage doivent être définies. Nous n'avons pas eu à gérer ce cas, mais nous pensons que la solution la plus sécurisée dans ce cas passera par un réseau Wireguard préconfiguré sur les machines, centralisant tout le trafic vers un serveur central (qui ne le redirige pas vers l'extérieur !).

À l'agrégation d'informatique 2022, nous avons utilisé un VLAN (Ethernet) dédié avec un serveur central servant en NFSv4+krb5i les contenus des `/home` des candidats (un compte par candidat et par épreuve), sauvegardés sur ce serveur toutes les quelques minutes, servant également de DHCP & PXE.
