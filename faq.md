---
title: FAQ
layout: page
---

* Table des matières
{:toc}

## Pourquoi Ubuntu ?

Ubuntu est la distribution Linux la plus populaire, et parmi les plus simples à prendre en main, à la fois du point de vue de l'utilisatrice ou l'utilisateur, et celui de l'administratrice ou l'administrateur.

## Pourquoi xfce ?

Ubuntu utilise GNOME par défaut mais :

- **xfce** utilise peu de ressources, ce qui facilite son intégration dans des VM ou des PC parfois anciens ;
- visuellement, xfce ressemble davantage à un environnement W-i-n-d-o-w-s et est donc facilement prenable en main par des candidat⋅es non habitué·es à Linux.

## Comment regénérer et [adapter](custom.html) l'image ?

L'image que nous générons est générée par l'intégration continue (CI) de GitLab, dont le script est disponible [ici](https://gitlab.com/agreg-info/clef-agreg/-/blob/master/.gitlab-ci.yml). En quelques mots, la CI lance `scripts/build.sh` et pousse l'image obtenue (en deux heures) sur un serveur.

Le script `build.sh` lance une installation standard d'Ubuntu 22.04 avec un fichier [`cloud-config.yaml`](https://gitlab.com/agreg-info/clef-agreg/-/blob/master/cloud-config.yaml) qui notamment contient les packages prérequis à l'installation (vous pouvez éditer ce fichier en fonction des paquets dont vous avez besoin).

Cette configuration a l'avantage de permettre un déploiement massif facile sur un réseau donné avec PXE, par exemple.

## Est-ce que Bépo est disponible ?

Oui, vous pouvez utiliser `setxkbmap`.

## ???

!!!

Les questions spécifiques à la clé peuvent être posées à [cle-agreg-info@groupes.renater.fr](mailto:cle-agreg-info@groupes.renater.fr).
