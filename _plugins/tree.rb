module Jekyll
  module Tree
    def tree(folder)
      val = folder + "\n"
      val += fun(folder, [])
      return val
    end

    def namedtree(folder, name)
      val = name + "\n"
      val += fun(folder, [])
      return val
    end

    def fun(where, prefixlist)
      val = ""
      listcontents = Dir.entries(where).delete_if{|i| i=="." or i==".." }
      last = listcontents.pop()
      if last == nil
        return ""
      end
      for file in listcontents do
        for i in prefixlist do
          if i
            val+= "│  "
          else
            val+= "   "
          end
        end
        val+= "├──"+file+"\n"
        if Dir.exists?(where+"/"+file)
          prefixlist << true
          val += fun(where+"/"+file, prefixlist)
          prefixlist.pop()
        end
      end
      for i in prefixlist do
        if i
          val+= "│  "
        else
          val+= "   "
        end
      end
      val+= "└──"+last+"\n"
      if Dir.exists?(where+"/"+last)
        prefixlist << false
        val += fun(where+"/"+last, prefixlist)
        prefixlist.pop()
      end
      return val
    end
  end
end

Liquid::Template::register_filter(Jekyll::Tree)
#usage {{ "my-folder/" | tree }}
